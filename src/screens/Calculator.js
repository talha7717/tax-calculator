import React, { useState } from "react";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import {
  Box,
  Button,
  Container,
  TextField,
  MenuItem,
  FormControl,
  Select,
  InputLabel,
} from "@mui/material";
import CalculateOutlinedIcon from "@mui/icons-material/CalculateOutlined";

const Calculator = () => {
  const [income, setIncome] = useState(0);
  const [isMonthly, setIsMonthly] = useState(true);
  const [taxAmountMonthly, setTaxAmountMonthly] = useState(0);
  const [afterDeductionMonthly, setAfterDeductionMonthly] = useState(0);
  const [taxAmountYearly, setTaxAmountYearly] = useState(0);
  const [afterDeductionYearly, setAfterDeductionYearly] = useState(0);

  const calculateIncomeTax = (annualIncome) => {
    if (annualIncome <= 600000) {
      return 0;
    } else if (annualIncome <= 1200000) {
      const taxableAmount = annualIncome - 600000;
      return (2.5 / 100) * taxableAmount;
    } else if (annualIncome <= 2400000) {
      const taxableAmount = annualIncome - 1200000;
      return 15000 + (12.5 / 100) * taxableAmount;
    } else if (annualIncome <= 3600000) {
      const taxableAmount = annualIncome - 2400000;
      return 165000 + (22.5 / 100) * taxableAmount;
    } else if (annualIncome <= 6000000) {
      const taxableAmount = annualIncome - 3600000;
      return 435000 + (27.5 / 100) * taxableAmount;
    } else {
      // For incomes above 6000000
      const taxableAmount = annualIncome - 6000000;
      return 1095000 + (35 / 100) * taxableAmount;
    }
  };

  const calculateTax = () => {
    const yearlyIncome = isMonthly ? income * 12 : income;
    const yearlyTaxAmount = calculateIncomeTax(yearlyIncome);

    if (isMonthly) {
      const monthlyTaxAmount = yearlyTaxAmount / 12;
      const monthlyAfterDeduction = income - monthlyTaxAmount;

      setTaxAmountMonthly(monthlyTaxAmount);
      setAfterDeductionMonthly(monthlyAfterDeduction);
      setTaxAmountYearly(yearlyTaxAmount);
      setAfterDeductionYearly(yearlyIncome);
    } else {
      if (yearlyTaxAmount === 0) {
        setTaxAmountMonthly(0);
        setAfterDeductionMonthly(yearlyIncome / 12);
        setTaxAmountYearly(0);
        setAfterDeductionYearly(yearlyIncome);
      } else {
        const monthlyTaxAmount = yearlyTaxAmount / 12; // Calculate monthly tax amount from yearly tax amount
        const monthlyAfterDeductionYearly = yearlyIncome - yearlyTaxAmount;

        setTaxAmountMonthly(monthlyTaxAmount);
        setAfterDeductionMonthly(monthlyAfterDeductionYearly / 12); // Divide yearly after deduction by 12 to get monthly after deduction
        setTaxAmountYearly(yearlyTaxAmount);
        setAfterDeductionYearly(monthlyAfterDeductionYearly);
      }
    }
  };

  return (
    <Box sx={{ p: 1 }}>
      <Container maxWidth="xs" sx={{ boxShadow: 3, mt: 10, py: 5 }}>
        <Stack direction="column">
          <Stack textAlign="center">
            <Typography sx={{ typography: { sm: "h4", xs: "h5" } }}>
              Income Tax Calculator 2023 – 24
            </Typography>
          </Stack>
          <Stack
            spacing={{ xs: 1, sm: 2 }}
            // direction="row"
            useFlexGap
            flexWrap="wrap"
          >
            <Stack
              direction={{ xs: "column", sm: "row" }}
              spacing={{ xs: 1, sm: 2, md: 4 }}
              //spacing={{ xs: 1, sm: 2 }}
              //direction="row"
              useFlexGap
              flexWrap="wrap"
            >
              <TextField
                sx={{ minWidth: 160 }}
                type="number"
                value={income}
                onChange={(e) => setIncome(parseFloat(e.target.value))}
                margin="normal"
                variant="outlined"
                label="Income"
                placeholder="Please enter the income"
                required
                size="small"
                color="success"
              />
              <FormControl sx={{ minWidth: 160 }} size="small" margin="normal">
                <InputLabel>Monthly/yearly</InputLabel>
                <Select
                  label="Monthly/yearly"
                  value={isMonthly ? "Monthly" : "Yearly"}
                  onChange={() => setIsMonthly(!isMonthly)}
                >
                  <MenuItem value={"Monthly"}>Monthly </MenuItem>
                  <MenuItem value={"Yearly"}>Yearly</MenuItem>
                </Select>
              </FormControl>
            </Stack>
            <Button
              variant="contained"
              size="small"
              color="success"
              onClick={calculateTax}
            >
              <CalculateOutlinedIcon sx={{ mr: 2 }} size="small" />
              Calculate Tax
            </Button>
          </Stack>

          {/* Display Monthly Calculation */}
          <Stack direction="column" mt={4} spacing={1.5}>
            <Typography sx={{ typography: { sm: "h5", xs: "h6" } }}>
              Monthly Calculation:
            </Typography>

            <Stack direction="row" justifyContent="space-between">
              <Stack
                sx={{
                  typography: { sm: "body1", xs: "body2" },
                  fontWeight: "bold",
                }}
              >
                Income Amount:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(!isMonthly ? income / 12 : income)}
              </Stack>
            </Stack>

            <Stack
              direction="row"
              justifyContent="space-between"
              color="#dd2c00"
            >
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                Tax Amount:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(taxAmountMonthly)}
              </Stack>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                Amount after Deduction:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(afterDeductionMonthly)}
              </Stack>
            </Stack>
          </Stack>

          {/* Display Yearly Calculation */}
          <Stack direction="column" mt={4} spacing={1.5}>
            <Typography sx={{ typography: { sm: "h5", xs: "h6" } }}>
              Yearly Calculation:
            </Typography>

            <Stack direction="row" justifyContent="space-between">
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                Income Amount:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(isMonthly ? income * 12 : income)}
              </Stack>
            </Stack>

            <Stack
              direction="row"
              justifyContent="space-between"
              color="#dd2c00"
            >
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                Tax Amount:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(taxAmountYearly)}
              </Stack>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                Amount after Deduction:
              </Stack>
              <Stack sx={{ typography: { sm: "body1", xs: "body2" } }}>
                {Math.ceil(afterDeductionYearly)}
              </Stack>
            </Stack>
          </Stack>
        </Stack>
      </Container>
    </Box>
  );
};

export default Calculator;
