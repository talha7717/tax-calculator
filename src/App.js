import React, { useState } from "react";

const App = () => {
  const [income, setIncome] = useState(0);
  const [isMonthly, setIsMonthly] = useState(true);
  const [taxAmountMonthly, setTaxAmountMonthly] = useState(0);
  const [afterDeductionMonthly, setAfterDeductionMonthly] = useState(0);
  const [taxAmountYearly, setTaxAmountYearly] = useState(0);
  const [afterDeductionYearly, setAfterDeductionYearly] = useState(0);

  const calculateIncomeTax = (annualIncome) => {
    if (annualIncome <= 600000) {
      return 0;
    } else if (annualIncome <= 1200000) {
      const taxableAmount = annualIncome - 600000;
      return (2.5 / 100) * taxableAmount;
    } else if (annualIncome <= 2400000) {
      const taxableAmount = annualIncome - 1200000;
      return 15000 + (12.5 / 100) * taxableAmount;
    } else if (annualIncome <= 3600000) {
      const taxableAmount = annualIncome - 2400000;
      return 165000 + (22.5 / 100) * taxableAmount;
    } else if (annualIncome <= 6000000) {
      const taxableAmount = annualIncome - 3600000;
      return 435000 + (27.5 / 100) * taxableAmount;
    } else {
      // For incomes above 2400000
      const taxableAmount = annualIncome - 6000000;
      return 1095000 + (35 / 100) * taxableAmount;
    }
  };

  const calculateTax = () => {
    const yearlyIncome = isMonthly ? income * 12 : income;
    const yearlyTaxAmount = calculateIncomeTax(yearlyIncome);
    const monthlyTaxAmount = yearlyTaxAmount / 12;

    setTaxAmountMonthly(monthlyTaxAmount);
    setAfterDeductionMonthly(income - monthlyTaxAmount);
    setTaxAmountYearly(yearlyTaxAmount);
    setAfterDeductionYearly(yearlyIncome - yearlyTaxAmount);
  };

  return (
    <div>
      <label>
        Enter your income:
        <input
          type="number"
          value={income}
          onChange={(e) => setIncome(parseFloat(e.target.value))}
        />
      </label>
      <label>
        <input
          type="radio"
          value="monthly"
          checked={isMonthly}
          onChange={() => setIsMonthly(true)}
        />
        Monthly Income
      </label>
      <label>
        <input
          type="radio"
          value="yearly"
          checked={!isMonthly}
          onChange={() => setIsMonthly(false)}
        />
        Yearly Income
      </label>

      <button onClick={calculateTax}>Calculate Tax</button>

      {/* Display Monthly Calculation */}
      <h3>Monthly Calculation:</h3>
      <p>Income Amount: {income}</p>
      <p>Tax Amount: {taxAmountMonthly}</p>
      <p>Amount after Deduction: {afterDeductionMonthly}</p>

      {/* Display Yearly Calculation */}
      <h3>Yearly Calculation:</h3>
      <p>Income Amount: {isMonthly ? income * 12 : income}</p>
      <p>Tax Amount: {taxAmountYearly}</p>
      <p>Amount after Deduction: {afterDeductionYearly}</p>
    </div>
  );
};

export default App;
